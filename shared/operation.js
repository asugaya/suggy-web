const fs = require("fs");
const path = require("path");

class Operation {

	get name(){ return this._name; }

	constructor() {

		this._durationPerformed = 0;
		this._timerStartTime = null;
	}

	init(){
		//Load config first
		this.listenForConfigChanges();

		//Only listen for changes if we're set up to handle it
		if(typeof this._onFileChanged !== "undefined"){
			this.listenForFileChanges();
		}

		//We would usually use === "undefined" above to return early for cleanliness, but in this case, we may want to do more stuff here
		
	}

	async setAppServer(app, server){
		this.app = app;
		this.server = server;
	}

	_updateConfig(){
		this._config = this.readFileAsJson("../config.json");
		this._name = this.config.name;
	}

	listenForConfigChanges(){
		const thiz = this;
		fs.watch(path.join(this.directory), function (event, filename) {
			if(filename !== "config.json")return;
			try{
				thiz._updateConfig();
			}catch(e){
				console.log(e);
			}
		});

		//We'll call this once to init as necessary
		thiz._updateConfig();
	}

	listenForFileChanges(){
		const thiz = this;
		fs.watch(path.join(this.directory, "files"), function (event, filename) {
			try{
				thiz._onFileChanged(filename);
			}catch(e){
				console.log(e);
			}
		});

		//We'll call this once to init as necessary
		this._onFileChanged();//We pass in undefined to signify that all files have changed
	}

	startDurationTimer(){
		this._timerStartTime = new Date();
	}

	stopDurationTimer(){
		if(this._timerStartTime === null)return;
		const duration = Math.abs(new Date() - this._timerStartTime);
		this._durationPerformed += duration;
		this._timerStartTime = null;
	}

	resetDurationPerformed(){
		this._durationPerformed = 0;
	}

	get durationPerformed(){
		return this._durationPerformed;
	}
	
	_read(directory, filename, defaultValue=""){
		try{
			const filepath = path.join(this.directory, directory, filename);
			return fs.readFileSync(filepath, { encoding: "utf8" });
		}catch(e){
			return defaultValue;
		}
	}

	readFile(filename, defaultValue=""){
		return this._read("files", filename, defaultValue);
	}

	readFileAsJson(filename, defaultValue="{}"){
		const jsonFile = this.readFile(filename, defaultValue);
		return JSON.parse(jsonFile);
	}

	get config(){
		return this._config;
	}

	set directory(val){
		this._directory = val;
	}

	get directory(){
		return this._directory;
	}

}

module.exports = Operation;