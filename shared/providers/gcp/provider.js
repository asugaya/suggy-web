const Provider = require("../provider.js");
const querystring = require("querystring");

const RandomString = require("randomstring");

const SERVICE_ACCOUNT_NAME = "SWeb Service Account";

const APIS = ["compute.googleapis.com"];

const compute = require("@google-cloud/compute");

const STARTUP_SCRIPT = "#!/bin/bash\nchmod +x /home/sweb/startup.sh\nsudo -H -u sweb bash -c /home/sweb/startup.sh\n";

try{
	var google = require("googleapis");
	var OAuth2 = google.auth.OAuth2;
}catch(e){
	console.log("Google APIs not found; this is ok as long as we aren't running locally on a console.");
}

class GCPProvider extends Provider{

	get name(){ return "GCPProvider"; }

	//app is the express app
	constructor(sutils, app){
		super(sutils);

		this._oAuthRequests = {};//This is a map of unique string ids to the promise for fulfilling those requests
		
		this.listenForOAuthCode(app);
	}

	//region API Call helpers
	async apiGet(...args){
		return await this.apiCall("GET", ...args);
	}

	async apiPost(...args){
		return await this.apiCall("POST", ...args);
	}

	async apiPut(...args){
		return await this.apiCall("PUT", ...args);
	}

	async apiDelete(...args){
		return await this.apiCall("DELETE", ...args);
	}

	async apiCall(method, token, host, path, data){
		try{
			const headers = {
				"Authorization": "Bearer " + token
			};

			return this.sutils.httpsRequest(method, headers, host, path, data);
		}catch(err){
			console.log(err);
			return err;
		}
	}
	//endregion

	
	//region OAuthToken and Access Key methods
	listenForOAuthCode(app){
		const thiz = this;

		app.get("", async function (req, res) {
			const requestInfo = thiz._oAuthRequests[req.query.state];

			if(typeof req.query.error !== "undefined"){
				requestInfo.reject(req.query.error);
			}

			res.send("Authorization successful! Feel free to close this window.");
			//TODO: have this use the new oauth client
			const user = requestInfo.user;
			const oauth2Client = user._gcpClient;

			oauth2Client.getToken(req.query.code, function (err, tokens) {
			// Now tokens contains an access_token and an optional refresh_token. Save them.
				if (!err) {
					oauth2Client.setCredentials(tokens);
					requestInfo.fulfill(tokens);
				}
			});
			/*
			//we'll use this code to get our auth token
			const queryDict = {
				"code":req.query.code
				, "client_id": CLIENT_ID
				, "client_secret": CLIENT_SECRET
				, "redirect_uri": thiz._redirectUrl
				, "grant_type": "authorization_code"
				, "code_verifier": requestInfo["code"]
			};
			var fullQueryString = querystring.stringify(queryDict);
			const path = `/oauth2/v4/token?${fullQueryString}`;
			const tokenResponse = await thiz.sutils.httpsRequest("POST", {}, "www.googleapis.com", path);
			console.log(tokenResponse);
			requestInfo.fulfill(tokenResponse);
			*/
			
		});
	}


	async getOAuthToken(user){
		//User is an object with a field "tokens"
		const thiz = this;

		return new Promise(async function(fulfill, reject){
			const requestId = RandomString.generate(128);
			const code_verifier = RandomString.generate(128);
			const loginUrl = await thiz.getOAuthCodeUrl(user, code_verifier, requestId);
			require("openurl").open(loginUrl);
			thiz._oAuthRequests[requestId] = {fulfill:fulfill, reject:reject, user:user/*, code:code_verifier*/};
		});
	}

	async getOAuthCodeUrl(user, code_verifier, requestId){
		const SCOPES = ["https://www.googleapis.com/auth/cloud-platform", "https://www.googleapis.com/auth/compute"];
		
		const url = user._gcpClient.generateAuthUrl({
			// 'online' (default) or 'offline' (gets refresh_token)
			access_type: "offline"

			// If you only need one scope you can pass it as a string
			, scope: SCOPES

			// Optional property that passes state parameters to redirect URI
			, state: requestId
		});

		return url;
		/*
		const gcpCreds = await this.sutils.readCredentials("gcp");
		const client_id = gcpCreds.client_id;
		const queryDict = {
			"client_id":client_id
			, "redirect_uri":redirect_uri
			, "response_type": "code"
			, "scope": SCOPES.join(" ")
			, "code_challenge_method":"plain"
			, "code_challenge":code_verifier
			, "state":requestId
		};
	
		var fullQueryString = querystring.stringify(queryDict);
		if(fullQueryString !== "")fullQueryString = "?" + fullQueryString;
	
		return  `http://accounts.google.com/o/oauth2/v2/auth${fullQueryString}`;
		*/
	}

	async getObjectByFilter(token, filterFunction, host, path, objectField){
		const response = await this.apiGet(token, host, path);
		const objects = response[objectField];
		if(typeof objects === "undefined")return null;
		var returnObject = null;
		objects.forEach(function(object){
			if(!filterFunction(object))return;
			returnObject = object;
		});
		return returnObject;
	}

	async getProjectByFilter(token, filterFunction){
		const host = "cloudresourcemanager.googleapis.com";
		const path = "/v1/projects";
		return await this.getObjectByFilter(token, filterFunction, host, path, "projects");
		
	}

	async getProjectByName(token, projectName){
		return await this.getProjectByFilter(token, function(project){
			if(project.name !== projectName)return false;
			if(project.lifecycleState !== "ACTIVE")return false;
			return true;
		});
	}

	async getProjectById(token, projectId){
		return await this.getProjectByFilter(token, function(project){
			if(project.projectId !== projectId)return false;
			if(project.lifecycleState !== "ACTIVE")return false;
			return true;
		});
	}

	async generateUniqueProjectId(token){
		var projectId = "";
		var isUnique = false;
		while(!isUnique){ //We could have done this recursively, but didn't want to risk the possibility of a stack overflow
			projectId = "sweb-" + RandomString.generate(11).toLowerCase();
			const project = await this.getProjectById(token, projectId);
			if(project === null)isUnique = true;
		}
		return projectId;

	}


	async waitUntilOperationIsComplete(token, operation, host){
		const thiz = this;
		const operationName = operation.name;
		const operationsPath = "/v1/"+operationName;

		return new Promise(async function(fulfill, reject){
			try{
				const checkIfOperationIsComplete = async function(){
					const operationResult = await thiz.apiGet(token, host, operationsPath);
					const operationComplete = operationResult.done;
					if(operationComplete)fulfill();
					else setTimeout(checkIfOperationIsComplete, 3000);
				};
				
				checkIfOperationIsComplete();

			}catch(e){
				reject(e);
			}
		});
	}

	async createProjectWithName(token, projectName){
		const host = "cloudresourcemanager.googleapis.com";
		const projectsPath = "/v1/projects";
		const projectId = await this.generateUniqueProjectId(token);
		const data = {
			"name": projectName
			, "projectId": projectId
		};
		const operation = await this.apiPut(token, host, projectsPath, data);
		await this.waitUntilOperationIsComplete(token, operation, host);
		
		const project = await this.getProjectByName(token, projectName);
		
		return project;

	}

	async getOrCreateProjectByName(token, projectName){
		var project = await this.getProjectByName(token, projectName);
		if(project == null){
			project = await this.createProjectWithName(token, projectName);
		} 
		
		return project;
	}

	async getServiceAccountByFilter(token, project, filterFunction){
		const host = "iam.googleapis.com";
		const path = "/v1/projects/"+project.projectId+"/serviceAccounts";
		return await this.getObjectByFilter(token, filterFunction, host, path, "accounts");		
	}

	async getServiceAccountByName(token, project, name){
		return await this.getServiceAccountByFilter(token, project, function(serviceAccount){
			if(serviceAccount.displayName !== name)return false;
			return true;
		});
	}

	async setServiceAccountAsOwner(token, project, serviceAccount){
		//First, get the existing policy:
		//https://cloudresourcemanager.googleapis.com/v1beta1/projects/$our-project-123:getIamPolicy
		const host = "cloudresourcemanager.googleapis.com";
		const getPath = `/v1/projects/${project.projectId}:getIamPolicy`;
		const existingPolicy = await this.apiPost(token, host, getPath);

		const bindings = existingPolicy.bindings;
		var editorBinding;
		for(let binding of bindings){
			if(binding.role === "roles/editor"){
				editorBinding = binding;
			}
		}

		if(typeof editorBinding === "undefined"){
			editorBinding = {
				role: "roles/editor"
				, members: []
			};
			bindings.push(editorBinding);
		}

		var updated = false;

		if(!editorBinding.members.includes(serviceAccount.email)){
			updated = true;
			editorBinding.members.push("serviceAccount:"+serviceAccount.email);
		}
		if(updated){
			const postPath = `/v1/projects/${project.projectId}:setIamPolicy`;
			const newPolicy = {
				"policy": {
					"bindings": bindings
				}
			};
			await this.apiPost(token, host, postPath, newPolicy);
		}

	}

	async getOrCreateServiceAccountForProject(token, project){
		var returnAccount = await this.getServiceAccountByName(token, project, SERVICE_ACCOUNT_NAME);
		if(returnAccount === null){
			const host = "iam.googleapis.com";
			const projectsPath = "/v1/projects/"+project.projectId+"/serviceAccounts";
			const data = {
				"accountId": project.projectId + "-sa",
				"serviceAccount": {
					"displayName": SERVICE_ACCOUNT_NAME,
				}
			};
			returnAccount = await this.apiPost(token, host, projectsPath, data);
		}
		await this.setServiceAccountAsOwner(token, project, returnAccount);

		return returnAccount;
	}
	async deleteKey(token, key){
		const host = "iam.googleapis.com";
		const path = `/v1/${key.name}`;
		return await this.apiDelete(token, host, path);
		
	}

	async deleteKeysForAccount(token, serviceAccount){
		const thiz = this;
		const host = "iam.googleapis.com";
		const path = `/v1/${serviceAccount.name}/keys`;
		const keysResponse = await this.apiGet(token, host, path);
		const keys = keysResponse.keys;
		if(typeof keys === "undefined")return;
		for(let key of keys){
			try{
				await thiz.deleteKey(token, key);
			}catch(e){}
		}
	}

	async createKeysForServiceAccount(token, serviceAccount){
		const thiz = this;
		return new Promise(async function(fulfill){
			const host = "iam.googleapis.com";
			const path = `/v1/${serviceAccount.name}/keys`;

			const key = await thiz.apiPost(token, host, path);

			const validAfterTime = new Date(key.validAfterTime);
			const nowTime = new Date();

			const timeTillLive = nowTime.getTime() - validAfterTime.getTime();
			console.log("Time till live:");
			console.log(timeTillLive);
			console.log(key.privateKeyData);
			const TIME_TILL_LIVE_BUFFER = 500;

			const buf = new Buffer(key.privateKeyData, "base64");
			const actualKey = JSON.parse(buf.toString("ascii"));
			setTimeout(function(){
				fulfill(actualKey);
			}, timeTillLive+TIME_TILL_LIVE_BUFFER);

			
		});
	}

	async enableAllAPIs(token, project) {
		//Generally, we may want this to only occur when we create projects, otherwise it's inefficient. That said, this allows for retroactive API activation (As opposed to having to recreate the project)
		//Enable any necessary APIs here
		for(let api of APIS){
			await this.enableAPIForProject(token, project, api);
		}
	}

	async enableAPIForProject(token, project, apiName){
		//https://servicemanagement.googleapis.com/v1/services/endpointsapis.appspot.com:enable
		const host = "servicemanagement.googleapis.com";
		const path = `/v1/services/${apiName}:enable`;
		const data = {
			"consumerId":`project:${project.projectId}`
		};

		const operation = await this.apiPost(token, host, path, data);
		await this.waitUntilOperationIsComplete(token, operation, host);
		
	}

	async promptForDefaultBilling(token){
		const host = "cloudbilling.googleapis.com";
		const path = "/v1/billingAccounts";

		console.log("Getting default billing.");

		const billingInfoData = await this.apiGet(token, host, path);
		const billingAccounts = billingInfoData.billingAccounts;
		var question = "Which billing account would you like to use?";
		var prompt = "[";
		var i = 0;
		for(let account of billingAccounts){
			const displayName = account.displayName;
			console.log(`${i}) ${displayName}\n`);
			if(i !== 0)prompt += ",";
			prompt += i;
			i += 1;

		}

		prompt += "]";
		console.log(prompt);
		const answer = await this.sutils.prompt(question, ["choice"]);
		console.log(answer);
		const answerInt = parseInt(answer.question);
		if(isNaN(answerInt) || answerInt > billingAccounts.length - 1 ){
			console.log("Bad input for billing account");
			return await this.promptForDefaultBilling(token);
		}
		//TODO: we should probably do some more error handling
		return billingAccounts[answerInt];

	}

	async setBillingForProject(token, defaultBilling, project){
		console.log(defaultBilling);
		const host = "cloudbilling.googleapis.com";
		const path = `/v1/projects/${project.projectId}/billingInfo`;
		var billingInfo = await this.apiGet(token, host, path);
		billingInfo.billingAccountName = defaultBilling.name;
		console.log(billingInfo);
		await this.apiPut(token, host, path, billingInfo);
	}

	async getAccessKeyForConsole(projectName){

		//We'll create a user that will be used for the oauth chain
		const user = {
			tokens:{}
		};

		const credentials = await this.sutils.readCredentials("gcp");
		const clientId = credentials.client_id;
		const clientSecret = credentials.client_secret;
		const config = this.sutils.config;
		const redirectUrl = config.redirectUri;

		//TODO: check to see if the user already has an oauth2Client/gcpClient
		var oauth2Client = new OAuth2(
			clientId
			, clientSecret
			, redirectUrl
		);

		user._gcpClient = oauth2Client;
		
		//We have to:
		//0 get the oauth token
		//1 create a project if one doesn't already exist
		//2 create a service account
		//3 get the keys for the account
		
		//Get the oauth token
		const token = await this.getOAuthToken(user);

		const access_token = token.access_token;

		const defaultBilling = await this.promptForDefaultBilling(access_token);

		//Create or get the project
		const project = await this.getOrCreateProjectByName(access_token, projectName);
		await this.enableAllAPIs(access_token, project);
		await this.setBillingForProject(access_token, defaultBilling, project);

		//Create or get the service account
		const serviceAccount = await this.getOrCreateServiceAccountForProject(access_token, project);
		
		//Delete any existing keys
		await this.deleteKeysForAccount(access_token, serviceAccount);

		//Create the keys! Yay.
		const keys = await this.createKeysForServiceAccount(access_token, serviceAccount);

		return keys;
	}
	//endregion
	

	//region GCP cloud APIs
	//This allows the developer to make calls like "provider.machine.create(foo, bar);"
	get machine() {
		//TODO: Add sshKeyInfo to metadata
		const thiz = this;
		return {
			create: async function(sshKeyInfo, machineName, machineSize, machineZone, machineSubzone, credentialsPath){
				console.log("Credentials path:");
				console.log(credentialsPath);
				const gce = compute({
					keyFilename: credentialsPath
				});

				const zoneString = thiz.convertZoneToString(machineZone, machineSubzone);
				const machineTypeString = thiz.convertMachineSizeToString(machineSize);

				// Create a new VM using the latest OS image of your choice.
				const zone = gce.zone(zoneString);
				const vm = zone.vm(machineName);
				
				const config = { 
					os: "debian"
					, machineType: machineTypeString
					, http: true
					, https: true
					, "metadata": {
						"items": [
							{
								"key": "ssh-keys",
								"value": `${sshKeyInfo.user}:${sshKeyInfo.pubkey}`
							}
							, {
								"key": "startup-script",
								"value": STARTUP_SCRIPT
							}
						]
					}
				};
				
				return vm.create(config).then(function(data){
					// `operation` lets you check the status of long-running tasks.
					console.log("Creating VM");

					const operation = data[1];

					return operation.promise();
				}).then(function(){
					console.log("Created VM!");
					const options = {
						timeout: 600
					};
					return vm.waitFor("RUNNING", options);
				}).then(function(data){
					const metadata = data[0];
					
					console.log("Metadata:");
					console.log(metadata);
					console.log(metadata.networkInterfaces[0].accessConfigs[0]);
					const externalIp = metadata.networkInterfaces[0].accessConfigs[0].natIP;
					console.log(externalIp);

					return externalIp;
				}).then(function(externalIp){
					//Set as a static external IP
					const regionString = thiz.convertZoneToString(machineZone, -1);//-1 gets just the region
				
					const region = gce.region(regionString);
					const address = region.address(`${machineName}-ip`);

					const options = {
						"kind": "compute#address"
						, "address": externalIp
					};

					return address.create(options).then(function(data){
						const operation = data[1];
						return operation.promise();
					}).then(function(){
						return externalIp;
					});

				});
					
			}
			, destroy: async function(machineName, machineZone, machineSubzone, credentialsPath){
				const gce = compute({
					keyFilename: credentialsPath
				});

				const zoneString = thiz.convertZoneToString(machineZone, machineSubzone);
				
				// Create a new VM using the latest OS image of your choice.
				const zone = gce.zone(zoneString);
				const vm = zone.vm(machineName);
				vm.getMetadata().then(function(data) {
					const metadata = data[0];
					console.log(metadata.networkInterfaces[0].accessConfigs[0]);
					const externalIp = metadata.networkInterfaces[0].accessConfigs[0].natIP;
					console.log(externalIp);

					return externalIp;
				}).then(function(externalIp){
					const regionString = thiz.convertZoneToString(machineZone, -1);//-1 gets just the region
				
					const region = gce.region(regionString);
					const address = region.address(`${machineName}-ip`);

					return address.delete().then(function(){
						return externalIp;
					});
				}).then(function(externalIp){
					return vm.delete().then(function(data){
						// `operation` lets you check the status of long-running tasks.
						console.log("Destroying VM");

						const operation = data[0];

						return operation.promise().then(function(){
							console.log("Destroyed VM!");
							return externalIp;
						});
					});
				});
				
					
			}
			, removeSshKeys(credentialsPath, machineName, machineZone, machineSubzone){
				const gce = compute({
					keyFilename: credentialsPath
				});
				const zoneString = thiz.convertZoneToString(machineZone, machineSubzone);
					
				const zone = gce.zone(zoneString);
				const vm = zone.vm(machineName);

				
				const metadata = {
					"ssh-keys": ""
					, "startup-script": STARTUP_SCRIPT
				};
				//-
				// If the callback is omitted, we'll return a Promise.
				//-
				return vm.setMetadata(metadata).then(function(data) {
					const operation = data[0];
					return operation.promise();
				}).then(function(){
					console.log("Removed keys!");
				});
			}
			, async runStartupScript(externalIp, sshKeyUser, privKey, sshKeyPassphrase){
				return await this.sutils.sexec("google_metadata_script_runner --script-type startup", externalIp, sshKeyUser, privKey, sshKeyPassphrase);
			}
		};
	}

	convertZoneToString(zone, subzone){
		//If subzone isn't defined, we'll use a random integer as the subzone value
		if(typeof subzone === "undefined") subzone = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);

		var zoneString = "";
		var subzones;

		switch(zone){
		case Provider.ZONES.US_EAST:
			zoneString = "us-east1";
			subzones = ["b", "c", "d"];
			break;
		case Provider.ZONES.US_CENTRAL:
			zoneString = "us-central1";
			subzones = ["a", "b", "c", "f"];
			break;
		case Provider.ZONES.US_WEST:
			zoneString = "us-west1";
			subzones = ["a", "b", "c"];
			break;
		default:
			break;
		}

		if(zoneString === "")return zoneString;//Return empty if input is invalid

		if(subzone !== -1){
			//Otherwise, get our subzone and add it on
			zoneString = zoneString + "-" + subzones[subzone % subzones.length];
		}

		return zoneString;

	}

	convertMachineSizeToString(machineSize){
		switch(machineSize){
		case Provider.MACHINE_SIZES.MICRO:
			return "f1-micro";
		case Provider.MACHINE_SIZES.TINY:
			return "g1-small";
		case Provider.MACHINE_SIZES.SMALL:
			return "n1-standard-1";
		case Provider.MACHINE_SIZES.MEDIUM:
			return "n1-standard-2";
		case Provider.MACHINE_SIZES.LARGE:
			return "n1-standard-8";
		case Provider.MACHINE_SIZES.HUGE:
			return "n1-standard-64";
		default:
			return "";
		}
	}
	//endregion


	//region old code
	/*
	//This allows the developer to make calls like "provider.machine.create(foo, bar);"
	// --- MACHINE ---
	get machine() {
		const thiz = this;

		return {
			// -- CREATE -- 
			create: async function (projectId, name, machineSize, zone, subzone){
				const zoneString = thiz.convertZoneToString(zone);
				const projectZoneString = thiz.convertProjectZoneToString(projectId, zone, subzone);
				const machineSizeString = thiz.convertMachineSizeToString(machineSize);
				const machineTypeString = `${projectZoneString}/machineTypes/${machineSizeString}`;
				const host = "www.googleapis.com";
				const path = `/compute/v1/${projectZoneString}/instances`;
				const data = {
					"name":name
					, "machineType":machineTypeString
					, "zone": projectZoneString
					, "tags": {
						"items": [
							"http-server",
							"https-server"
						]
					}
					, "disks": [
						{
							"type": "PERSISTENT",
							"boot": true,
							"mode": "READ_WRITE",
							"autoDelete": true,
							"deviceName": name,
							"initializeParams": {
								"sourceImage": "projects/debian-cloud/global/images/debian-9-stretch-v20170918",
								"diskType": `${projectZoneString}/diskTypes/pd-standard`,
								"diskSizeGb": "10"
							}
						}
					],
					"canIpForward": false,
					"networkInterfaces": [
						{
							"network": `projects/${projectId}/global/networks/default`,
							"subnetwork": `projects/${projectId}/regions/${zoneString}/subnetworks/default`,
							"accessConfigs": [
								{
									"name": "External NAT",
									"type": "ONE_TO_ONE_NAT"
								}
							],
							"aliasIpRanges": []
						}
					],
					"description": "",
					"labels": {},
					"scheduling": {
						"preemptible": false,
						"onHostMaintenance": "MIGRATE",
						"automaticRestart": true
					},
					"serviceAccounts": [
					]
				};

				const result = await thiz.apiPost(host, path, data);
				return result;
				
			}
			// --- DELETE ---
			, delete: async function(projectId, zoneString, instanceName){
				const host = "www.googleapis.com";
				const path = `/compute/v1/projects/${projectId}/zones/${zoneString}/instances/${instanceName}`;
				
				const result = await thiz.apiDelete(host, path);
				return result;
			}
		};
	}

	setToken(token){
		this._token = token;
	}

	getOAuthCodeUrl(redirect_uri, code_verifier){
		const SCOPES = ["https://www.googleapis.com/auth/compute", "https://www.googleapis.com/auth/cloud-platform"];
	
		const queryDict = {
			"client_id":"852811352898-cnu06h6c7ql4li3ei03sf58ah2eep47v.apps.googleusercontent.com"
			, "redirect_uri":redirect_uri
			, "response_type": "code"
			, "scope": SCOPES.join(" ")
			, "code_challenge_method":"plain"
			, "code_challenge":code_verifier
		};
	
		var fullQueryString = querystring.stringify(queryDict);
		if(fullQueryString !== "")fullQueryString = "?" + fullQueryString;
	
		return  `http://accounts.google.com/o/oauth2/v2/auth${fullQueryString}`;
	}

	convertProjectZoneToString(projectId, zone, subzone){
		//If subzone isn't defined, we'll use a random integer as the subzone value
		if(typeof subzone === "undefined") subzone = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);

		var zoneString = "";
		var subzones;

		switch(zone){
		case Provider.ZONES.US_EAST:
			zoneString = "us-east1";
			subzones = ["b", "c", "d"];
			break;
		case Provider.ZONES.US_CENTRAL:
			zoneString = "us-central1";
			subzones = ["a", "b", "c", "f"];
			break;
		case Provider.ZONES.US_WEST:
			zoneString = "us-west1";
			subzones = ["a", "b", "c"];
			break;
		default:
			break;
		}

		if(zoneString === "")return zoneString;//Return empty if input is invalid

		//Otherwise, get our subzone and add it on
		zoneString = "projects/" + projectId + "/zones/" + zoneString + "-" + subzones[subzone % subzones.length];

		return zoneString;

	}

	convertZoneToString(zone){
		switch(zone){
		case Provider.ZONES.US_EAST:
			return "us-east1";
		case Provider.ZONES.US_CENTRAL:
			return "us-central1";
		case Provider.ZONES.US_WEST:
			return "us-west1";
		}
	}

	convertMachineSizeToString(machineSize){
		switch(machineSize){
		case Provider.MACHINE_SIZES.MICRO:
			return "f1-micro";
		case Provider.MACHINE_SIZES.TINY:
			return "g1-small";
		case Provider.MACHINE_SIZES.SMALL:
			return "n1-standard-1";
		case Provider.MACHINE_SIZES.MEDIUM:
			return "n1-standard-2";
		case Provider.MACHINE_SIZES.LARGE:
			return "n1-standard-8";
		case Provider.MACHINE_SIZES.HUGE:
			return "n1-standard-64";
		default:
			return "";
		}
	}
	*/
	//endregion
}


module.exports = GCPProvider;