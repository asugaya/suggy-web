//TODO: This is what function endpoints will reference to perform tasks like creations and updates
//All functions in this class will return a SuggyEntity
module.exports = function(sutils){

	const SuggyEntity = sutils.requireWithSutils("entity/entity.js");

	const EntityBroadcast = sutils.requireWithSutils("entity/entityBroadcast.js");

	class EntityDatabase {
		constructor(externalProjectKeys){//If externalApplication is specified, we'll write to an external database
			//We'll default to using the shared database for now
			const Database = sutils.require("database/database.js");
			this._db = new Database(sutils, externalProjectKeys);
			this._broadcast = new EntityBroadcast();
		}

		//The four essential functions: CRUD
		async create(kind, identifier, data){
			const kindInfo = sutils.getKindInfo(kind);
			if(typeof kindInfo === "undefined")throw "Kind does not exist: " + kind;
			
			if(typeof identifier === "object"){
				data = identifier;
				identifier = sutils.uuid();
			}

			if(typeof data === "undefined"){
				data = {};
			}
			//data._l = [];
			if(typeof data._r === "undefined"){
				data._r = {};
			}
			if(typeof identifier === "undefined")identifier = sutils.uuid();
			const returnData = await this._db.create(kind, identifier, data);
			const newEntity = new SuggyEntity(kindInfo, returnData, this);
			return newEntity;
		}

		async read(kind, identifier){
			const kindInfo = sutils.getKindInfo(kind);
			if(typeof kindInfo === "undefined")throw "Kind does not exist: " + kind;

			const returnData = await this._db.read(kind, identifier);

			//The can read check is performed by the entityEndpoint
			
			const newEntity = new SuggyEntity(kindInfo, returnData, this);
			return newEntity;
		}

		async update(entity){
			const entityKind = entity._kind;
			const entityId = entity._identifier;
			const kindInfo = sutils.getKindInfo(entityKind);
			if(typeof kindInfo === "undefined")throw "Kind does not exist: " + entityKind;
			
			//TODO: update listeners here
			const dirtyFields = entity.dirtyFields;
			const dirtyData = {};
			for(let field of dirtyFields){
				dirtyData[field] = entity._data[field];
			}

			console.log("Notifying listeners with data");
			console.log(dirtyData);
			await this._broadcast.notifyListeners(entityKind, entityId, {"type":"entity", "action":"update", "data":{"kind":entityKind, "id":entityId, "data":dirtyData}});
			

			//Update the data
			await this._db.update(entityKind, entityId, entity._data);

			entity.clearDirtyFields();
		}

		async delete(entity){
			const kind = entity._kind;
			const identifier = entity._identifier
			await this._db.delete(kind, identifier);

			await this._broadcast.notifyListeners(kind, identifier, {"type":"entity", "action":"delete", "data":{"kind":entity._kind, "id":entity._identifier}});
			
			//We don't care about updating the failed listeners, as the entity is now deleted anyway.
			// for(let failedListener of failedListeners){
			// 	const originIdIndex = listeners.indexOf(failedListener);
			// 	if(originIdIndex === -1)continue;//This shouldn't be the case, but just in case...
			// 	listeners.splice(originIdIndex, 1);
			// }
		}

		async querySingle(kind, filters){
			const kindInfo = sutils.getKindInfo(kind);
			if(typeof kindInfo === "undefined") throw "Kind does not exist: " + kind;
			const result = await this._db.querySingle(kind, filters);

			if(typeof result === "undefined")return result;
			const newEntity = new SuggyEntity(kindInfo, result, this);
			return newEntity;
		}

		/*
		//Subscription methods
		async subscribe(kind, identifier, user, originId, data){

			if(typeof data === "undefined"){
				data = await this._db.read(kind, identifier);
			}
			const listeners = data._l;
			if(listeners.indexOf(originId) !== -1)return;

			//if(!this.userCanRead(user, data))return;
			
			//Perform a clean every 10 or so times, to make sure we clear out dead listeners
			if(Math.random() < 0.1){
				const failedListeners = await this.notifyListeners(listeners);
				console.log("Failed listeners in subscribe: ");
				console.log(failedListeners);
				for(let failedListener of failedListeners){
					const originIdIndex = listeners.indexOf(failedListener);
					if(originIdIndex === -1)continue;//This shouldn't be the case, but just in case...
					listeners.splice(originIdIndex, 1);
				}
			}

			listeners.push(originId);

			await this._db.update(kind, identifier, data);
		}

		async unsubscribe(kind, identifier, user, originId){
			const data = await this._db.read(kind, identifier);
			const originIdIndex = data._l.indexOf(originId);
			if(originIdIndex === -1)return;
			data._l.splice(originIdIndex, 1);
			await this._db.update(kind, identifier, data);
		}

		//Helper functions
		async notifyListeners(listeners, message){
			console.log("Sending to listeners " + listeners);
			
			const failedListeners = await sutils.sendDataToOrigins(message, listeners);//We could await here, but we really don't care about the results--so long as it gets sent, we aren't waiting on a response
			
			return failedListeners;
		}
		*/

		calculateDeltas(oldData, newData){
			const deltas = [];
			const oldKeys = Object.keys(oldData);
			const newKeys = Object.keys(newData);

			//A field can either be deleted, added, or updated

			for(let oldKey of oldKeys){
				//If an old key doesn't exist in the new list, it's been deleted
				if(newKeys.indexOf(oldKey) === -1){
					deltas.push({"field":oldKey, "action":"deleted"});
				}
			}

			for(let newKey of newKeys){
				//If a new key doesn't exist in the old list, it's been added
				if(oldKeys.indexOf(newKey) === -1){
					deltas.push({"field":newKey, "action":"added"});
				}

				//If the new value doesn't match the old value, then it gets updated
				if(oldData[oldKey] === newData[newKey]){
					deltas.push({"field":newKey, "action":"updated"});
				}
			}

			return deltas;

		}

		//Permissions
		/*
		kindIdentifierCanRead(kind, identifier, data, field){
			const permissions = data._p;
			const kindIdentifier = kind + ":" + identifier;
			const currentPermission = permissions[kindIdentifier];
			return currentPermission === EntityDatabase.PERMISSIONS.READ;
		}

		userCanRead(user, data, field){
			const thiz = this;
			if(thiz.kindIdentifierCanReadField("user", user._identifier, data, field))return true;
			if(thiz.kindIdentifierCanReadField("*", "*", data, field))return true;//This is our global check
			return false;
		}
		
		addReadPermission(data, kindIdentifier){
			const permissions = data._p;
			permissions[kindIdentifier] = EntityDatabase.PERMISSIONS.READ;
		}

		removeReadPermission(data, kindIdentifier){
			const permission = data._p;
			//const currentPermission = permissions[kindIdentifier];
			//if(typeof currentPermission === "undefined")return;
			delete permissions[kindIdentifier];
		}

		addReadPermissionForUser(data, user){
			return this.addReadPermission(data, "sw-user:"+user._id);
		}

		removeUserReadPermissionForUser(data, user){
			return this.removeReadPermission(data, "sw-user:"+user._id);
		}
		*/
	}


	EntityDatabase.PERMISSIONS = Object.freeze({
		"READ": "r"
		//, "WRITE": "w"
		//, "READWRITE": "b"
	});

	return EntityDatabase;
}