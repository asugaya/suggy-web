const GoogleDBConnector = require("./googleDbConnector.js");

let singleton = null;

const Cache = require("./cache.js");

class Database {

	constructor(sutils, externalProjectKeys) {
		if(typeof externalProjectKeys !== "undefined"){
			this._init(sutils, externalProjectKeys);
			return this;
		}

		if(!singleton){
			singleton = this;
			this._init(sutils);
		}

		return singleton;

	}

	_init(sutils, externalProjectKeys){
		this._connectors = [];
		this._googleDb = new GoogleDBConnector(sutils, externalProjectKeys);
		this.addConnector(this._googleDb);
		this._cache = new Cache();
	}

	addConnector(connector){
		this._connectors.push(connector);
	}

	//TODO: for all of these, we have to decide which connector we'll want to use in the future
	//For now, default to the only connector we have
	async create(kind, identifier, data){
		const key = kind+":"+identifier;
		this._cache.set(key, data);
		//TODO: Do we want to limit a user's ability to create a certain kind of entity based on its data? e.g., a user shouldn't be able to reference any entities it doesn't have write access to.
		//Nah, it's ok. This will only be called internally now
		return this._connectors[0].create(kind, identifier, data);
	}

	async read(kind, identifier){
		const key = kind+":"+identifier;
		const cacheResult = await this._cache.get(key);
		if(typeof cacheResult !== "undefined"){
			console.log("USING CACHE :)");
			return cacheResult;
		}
		const dbResult = this._connectors[0].read(kind, identifier);
		await this._cache.set(key, dbResult);
		return dbResult;
	}

	async readBatch(kindIdentifiers){
		return this._connectors[0].readBatch(kindIdentifiers);
	}

	async update(kind, identifier, data){
		const key = kind+":"+identifier;
		await this._cache.set(key, data);
		return this._connectors[0].update(kind, identifier, data);
	}

	async delete(kind, identifier){
		const key = kind+":"+identifier;
		await this._cache.delete(key);
		return this._connectors[0].delete(kind, identifier);
	}

	async query(kind, filters){
		return this._connectors[0].query(kind, filters);
	}

	async querySingle(kind, filters){
		return this._connectors[0].querySingle(kind, filters);
	}
/*
	async readOrCreate(kind, identifier, data){
		var entity;
		try{
			entity = await this.read(kind, identifier);
		}catch(e){
			entity = await this.create(kind, identifier, data);
		}
		
		return entity;
	}

	async updateOrCreate(kind, identifier, data){
		var entity;
		try{
			entity = await this.update(kind, identifier, data);
		}catch(e){
			entity = await this.create(kind, identifier, data);
		}

		return entity;
	}
	*/

	getDatastore(session){
	
		const Store = session.Store;
		const database = this;
	
		class SWebDatastore extends Store {
			constructor(){
				super();

			}

			get(sid, fn){
				console.log("Get");
				console.log(sid);
				database.read("session", sid).then(function(sessionData){
					console.log(sessionData.sess);
					fn(null, JSON.parse(sessionData.sess));
				}).catch(function(err){
					fn(null);
				});
			}

			set(sid, sess, fn){
				console.log("Set");
				console.log(sid);
				const sessString = JSON.stringify(sess);
				console.log(sessString);
				
				database.update("session", sid, {"sess":sessString}).then(function(session){
					fn(null, session);
					console.log("Updated");
				}).catch(function(err){

					console.log("e1");
					console.log(err);
					return database.create("session", sid, {"sess":sessString}).then(function(session){
						fn(null, session);
					}).catch(function(err){
					console.log("e2");
					console.log(err);
						fn(err);
					});
				});
				
			}

			destroy(sid, fn){
				console.log("Destroy");
				database.delete("session", sid).then(function(){
					// console.log("Deleted");
					// console.log(sid);
					fn(null);
				}).catch(function(err){
					fn(err);
				});
			}
			/*
			touch(sid, sess, fn){
				console.log("Touch");
			}
			*/
		}

		return new SWebDatastore();
	}

		/**
		* Attempt to fetch session by the given `sid`.
		*
		* @param {String} sid
		* @param {Function} fn
		* @api public
		*/
/*
		RedisStore.prototype.get = function (sid, fn) {
		var store = this;
		var psid = store.prefix + sid;
		if (!fn) fn = noop;
		debug('GET "%s"', sid);

		store.client.get(psid, function (er, data) {
		if (er) return fn(er);
		if (!data) return fn();

		var result;
		data = data.toString();
		debug('GOT %s', data);

		try {
		result = store.serializer.parse(data);
		}
		catch (er) {
		return fn(er);
		}
		return fn(null, result);
		});
		};
*/
		/**
		* Commit the given `sess` object associated with the given `sid`.
		*
		* @param {String} sid
		* @param {Session} sess
		* @param {Function} fn
		* @api public
		*/
/*
		RedisStore.prototype.set = function (sid, sess, fn) {
		var store = this;
		var args = [store.prefix + sid];
		if (!fn) fn = noop;

		try {
		var jsess = store.serializer.stringify(sess);
		}
		catch (er) {
		return fn(er);
		}

		args.push(jsess);

		if (!store.disableTTL) {
		var ttl = getTTL(store, sess, sid);
		args.push('EX', ttl);
		debug('SET "%s" %s ttl:%s', sid, jsess, ttl);
		} else {
		debug('SET "%s" %s', sid, jsess);
		}

		store.client.set(args, function (er) {
		if (er) return fn(er);
		debug('SET complete');
		fn.apply(null, arguments);
		});
		};
		*/

		/**
		* Destroy the session associated with the given `sid`.
		*
		* @param {String} sid
		* @api public
		*/
/*
		RedisStore.prototype.destroy = function (sid, fn) {
		debug('DEL "%s"', sid);
		if (Array.isArray(sid)) {
		var multi = this.client.multi();
		var prefix = this.prefix;
		sid.forEach(function (s) {
		multi.del(prefix + s);
		});
		multi.exec(fn);
		} else {
		sid = this.prefix + sid;
		this.client.del(sid, fn);
		}
		};
		*/

		/**
		* Refresh the time-to-live for the session with the given `sid`.
		*
		* @param {String} sid
		* @param {Session} sess
		* @param {Function} fn
		* @api public
		*/
/*
		RedisStore.prototype.touch = function (sid, sess, fn) {
		var store = this;
		var psid = store.prefix + sid;
		if (!fn) fn = noop;
		if (store.disableTTL) return fn();

		var ttl = getTTL(store, sess);

		debug('EXPIRE "%s" ttl:%s', sid, ttl);
		store.client.expire(psid, ttl, function (er) {
		if (er) return fn(er);
		debug('EXPIRE complete');
		fn.apply(this, arguments);
		});
		};
*/
		/**
		* Fetch all sessions' Redis keys using non-blocking SCAN command
		*
		* @param {Function} fn
		* @api private
		*/
/*
		function allKeys (store, cb) {
		var keysObj = {}; // Use an object to dedupe as scan can return duplicates
		var pattern = store.prefix + '*';
		var scanCount = store.scanCount;
		debug('SCAN "%s"', pattern);
		(function nextBatch (cursorId) {
		store.client.scan(cursorId, 'match', pattern, 'count', scanCount, function (err, result) {
		if (err) return cb(err);

		var nextCursorId = result[0];
		var keys = result[1];

		debug('SCAN complete (next cursor = "%s")', nextCursorId);

		keys.forEach(function (key) {
		keysObj[key] = 1;
		});

		if (nextCursorId != 0) {
		// next batch
		return nextBatch(nextCursorId);
		}

		// end of cursor
		return cb(null, Object.keys(keysObj));
		});
		})(0);
		}*/

		/**
		* Fetch all sessions' ids
		*
		* @param {Function} fn
		* @api public
		*/
		/*
		RedisStore.prototype.ids = function (fn) {
		var store = this;
		var prefixLength = store.prefix.length;
		if (!fn) fn = noop;

		allKeys(store, function (err, keys) {
		if (err) return fn(err);

		keys = keys.map(function (key) {
		return key.substr(prefixLength);
		});
		return fn(null, keys);
		});
		};
		*/


		/**
		* Fetch all sessions
		*
		* @param {Function} fn
		* @api public
		*/
		/*
		RedisStore.prototype.all = function (fn) {
		var store = this;
		var prefixLength = store.prefix.length;
		if (!fn) fn = noop;

		allKeys(store, function (err, keys) {
		if (err) return fn(err);

		store.client.mget(keys, function (err, sessions) {
		if (err) return fn(err);

		var result;
		try {
		result = sessions.map(function (data, index) {
		data = data.toString();
		data = store.serializer.parse(data);
		data.id = keys[index].substr(prefixLength);
		return data;
		});
		} catch (e) {
		err = e;
		}

		return fn(err, result);
		});
		});
		};

		return RedisStore;
		*/
	



}

module.exports = Database;