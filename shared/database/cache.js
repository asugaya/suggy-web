//TODO: in the future, make sure this is distributed
//TODO: IMPORTANT ^
let singleton = null;

class Cache {

	constructor(sutils) {
		if(!singleton){
			singleton = this;
			this._init(sutils);
		}

		return singleton;
	}

	_init(sutils){
		const thiz = this;
		this._cache = {};
		//await sutils.listenForChangesToFile("graph", function(graph){thiz.updateServerEndpoints(graph);});
	}
	/*
	updateServerEndpoints(graph){
		const endpointsMap = {};
		graph.servers.forEach(function(server){

		});
	}
	*/

	async get(key) {
		return this._cache[key];
	}


	async set(key, data){
		this._cache[key] = data;
	}

	async delete(key){
		delete this._cache[key];
	}
}

module.exports = Cache;