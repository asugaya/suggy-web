module.exports = function(sutils){

	const Endpoint = require("./endpoint.js");
	const UglifyJS = require("uglify-es");

	class WebpageEndpoint extends Endpoint {

		constructor() {
			super();
			this._onFileChanged = this.reloadPageTemplate;
			this._templates = {};
			this.usesUser = false;//TODO: in the future, perhaps we could open this up to provide something different based on the user
		}

		reloadPageTemplate(){
			//TODO: we could load the specific file that changed (or all, if undefined) for efficiency if need be
			const js = this.readFile("script.js");
			const body = this.readFile("body.html");
			const head = this.readFile("head.html");
			const css = this.readFile("style.css");

			var appLibs = this.config.libs;
			if(typeof appLibs === "undefined") appLibs = [];

			const jsMinified = sutils.minifyJs(js);

			this._pageTemplate =  {"js":jsMinified, "alibs":appLibs, "head":head, "body":body, "css":css};
		}

		async process(data, originId){
			//This will default to grabbing tempates
			const action = data.action;
			if(action === WebpageEndpoint.TEMPLATE_ACTION){
				const templateName = data.inputs.template;
				var template = this._templates[templateName];
				if(typeof template !== "undefined")return template;
				template = this.getTemplateInfo(templateName);
				this._templates = template;

				return template;
			}else if(action === WebpageEndpoint.WEBPAGE_ACTION){
				return this._pageTemplate;
			}

		}

		getTemplateInfo(templateName, defaultValue=""){
			const fileTypes = ["js", "html", "css"];
			const templateInfo = {};
			for(let fileType of fileTypes){
				templateInfo[fileType] = this._read(`templates/${templateName}`, "template."+fileType, defaultValue);
			}
			templateInfo.js = sutils.minifyJs(templateInfo.js);

			return templateInfo;
		}

		_updateConfig(){
			super._updateConfig();
			this.resource = "w/"+this.config.resource;
		}

	}

	WebpageEndpoint.TEMPLATE_ACTION = "t";
	WebpageEndpoint.WEBPAGE_ACTION = "w";

	return WebpageEndpoint;
}