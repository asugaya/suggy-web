const Operation = require("../operation.js");

class Endpoint extends Operation {


	constructor() {

		super();
		
		//Create getters and setters for each of the verbs
		//the field will look like "this.postResponse"
		/* We don't really need this; children can simply define "postResponse as a function" and we'll use that
		Endpoint.VERBS.forEach(function(verb){
			const verbResponse = `${verb}Response`;
			const verbResponseField = `_${verbResponse}`;
			Object.defineProperty(this, verbResponse, {
				enumerable: true
				, configurable: true
				, get: function() {
					return this[verbResponseField];
				}
				, set: function(val){
					this[verbResponseField] = val;
				}
			});
		});
		*/

		//Then we're good to go!
	}

	_updateConfig(){
		super._updateConfig();
		this.resource = this.config.resource;
	}


	get resource(){
		return this._resource;
	}

	set resource(val){
		this._resource = val;
	}
}

//Endpoint.VERBS = ["post", "patch", "get", "delete", "put"];

module.exports = Endpoint;