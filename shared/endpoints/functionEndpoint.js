
module.exports = function(sutils){
	const Endpoint = require("./endpoint.js");

	const InputVariable = require("../variables/inputVariable.js");
	const OutputVariable = require("../variables/outputVariable.js");

	class FunctionEndpoint extends Endpoint {

		constructor() {

			super();
			this._inputVariables = [];
			this._outputVariable = null;
		}

		_updateConfig(){
			super._updateConfig();
			const thiz = this;

			const inputs = this.config.inputs;
			inputs.forEach(function(input){
				const newInput = new InputVariable(input.name, input.type, input.required);
				thiz.addInputVariable(newInput);
			});

			const output = this.config.output;
			const newOutput = new OutputVariable(output.name, output.type);
			this.setOutputVariable(newOutput);

			
			this.usesUser = this.config.usesUser;
			if(typeof this.usesUser === "undefined")this.usesUser = true;

			this.resource = "f/"+this.config.resource;
		}

		async process(data, originId, user, updateProgress){

			/*
				Subclasses should have a function called "ioFunction" that will get called with inputs

			 */
			/*
				Data should look like:
				{
					action: "function"
					, inputs: {
						"var1":"val1"
						, "var2": "val2"
					}
				}
			 */
			
			
			if(typeof this.ioFunction === "undefined") throw "Function has not been defined";
			
			if(data.action !== FunctionEndpoint.ACTION) throw "Incorrect action type: " + data.action;
			
			const inputs = {};
			this._inputVariables.forEach(function(iVar){
				//TODO: Validate/massage based on type
				const varName = iVar.name;
				const input = data.inputs[varName];
				if(iVar.isRequired && typeof input === "undefined") throw `Required input: ${varName}`;
				inputs[varName] = input;
			});

			const output = this.ioFunction(inputs, originId, user, updateProgress);
			//TODO: Validate outputs
			return output;
		}


		addInputVariable(variable){
			this._inputVariables.push(variable);
		}

		get inputVariables(){
			return this._inputVariables;
		}

		setOutputVariable(variable){
			this._outputVariable = variable;
		}

		get outputVariable(){
			return this._outputVariable;
		}

	}

	FunctionEndpoint.ACTION = "f";

	return FunctionEndpoint;
}