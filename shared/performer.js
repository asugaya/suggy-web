const Operation = require("./operation");

class Performer extends Operation {

	constructor(name) {
		super(name);
	}

	_updateConfig(){
		super._updateConfig();
		this.performPeriod = this.config.performPeriod || 0;//Default to 0
	}

	set app(val){
		this._app = val;
	}

	set server(val){
		this._server = val;
	}

	get app(){
		return this._app;
	}

	get server(){
		return this._server;
	}

	async perform(){

	}
}

module.exports = Performer;