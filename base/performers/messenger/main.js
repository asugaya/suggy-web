/*
    This is our messenger script. This code is responsible for:
        - Listening for requests from clients
        - Responses from endpoints
*/
//TODO: Make all cookies use secure = true
//const cookieParser = require("cookie-parser");
//const sharedSession = require("express-socket.io-session");
const jwt = require("jsonwebtoken");


module.exports = function(sutils) { //This allows us to not have to worry about hard-coding paths, as well as providing this handy utils object

	const Performer = sutils.require("performer.js");
	const Logger = sutils.require("logger.js");

	const SuggyEntity = sutils.requireWithSutils("entity/entity.js");

	var DatabaseClass = sutils.requireWithSutils("entity/entityDatabase.js");
	var database = new DatabaseClass();



	class MessengerPerformer extends Performer {
		
		constructor(){
			super();
		}

		async setAppServer(app, server, session){

			const config = sutils.config;

			Logger.log("MessengerPerformer", "Setting up websockets");
			const thiz = this;

			const port = config.secure ? "443" : "80";
			const options = {
				origins: config.domain+":"+port
				, transports: ["websocket"]
			};

			const io = require("socket.io")(server, options);
			this._io = io;

			const originToSocketMap = {};

			/*
			io.use(sharedSession(session 
				, cookieParser(sutils.config.secret, {secure: true})
				, {
					autoSave: true
				}
			));
			*/
			await sutils.listenForChangesToFile("graph", function(graph){thiz.updateServerEndpoints(graph);});

			//All SocketIO messages will have the format:
			/*
				{
					"action": /something like 'f' or 'c'/
					, "endpoint": /endpointName/
					/then, the rest depends on 
				}
			 */
			io.on("connection", function connection(socket) {

				function sendReady(){
					socket.emit("swrdy", {});
				}

				var originId = config.id + ":" + sutils.uuid();

				originToSocketMap[originId] = socket;

				var userId = null;
				var userSwt = null;

				//socket.setMaxListeners(20);  // Set the client's socket.io EE to max listeners: 20
				console.log("Listening to socket:");
				socket.on("swreq", async function incoming(data) {
					var response;
					try{
						console.log("received: %s", JSON.stringify(data));
						const endpointResource = data.endpoint;

						//Here we want to find the server that supports this endpoint
						const endpointServer  = thiz.getServerForEndpoint(endpointResource);

						const updateProgress = function(update){
							const returnData = {"result":update, "type":"simple"};//await sutils.getReturnData(update, userId);
							returnData.id = data.id;
							socket.emit("swupd", returnData);
						};

						//TODO: pass on the user id in a secure manner to the external endpoints
						if(endpointServer.address === config.address){
							response = await thiz.respondWithInternalEndpoint(originId, endpointResource, data, userId, updateProgress);
						}else{
							response = await thiz.respondWithExternalEndpoint(originId, endpointServer, endpointResource, data, userSwt, updateProgress);
						}
						socket.emit("swres", response);

					}catch(e){
						console.log("Error: %s", e);
						console.log(e);
					}
				});

				socket.on('disconnect', function() {
			      console.log('Got disconnect!');
			      delete originToSocketMap[originId];
			   });

				socket.on('swauth', function(data){
					const swt = data.swt;
					if(typeof swt === "undefined"){
						sendReady();
						return;
					}
					try{
						const decoded = jwt.verify(swt, config.secretTwo, {
							ignoreExpiration:true
							//TODO: make sure we only allow specific algorithms (ie, protect against algo: none)	
						});
						console.log("Decoded JWT to: ")
						console.log(decoded);
						const decodedUser = decoded.user;
						if(typeof decodedUser !== "undefined"){
							userId = decodedUser.id;
							userSwt = swt
						}
					}catch(err){
						//Client sent an invalid or expired token, simply tell them to reauth
						socket.emit('swlogout');
					}
					sendReady();

				});

				

			});


			//Now we set up a proxy to ferry messages from http to ws
			app.post("/_swmessenger", function(req, res, next){
				try{
					const data = req.body;
					if(typeof req.headers.origin !== "undefined" & !sutils.idMatchesAddress(data.origin, req.headers.origin)){
						console.log("Origin does not match: " + data.origin + " " + req.headers.origin);
						res.send({"success":false, "e":"Origin mismatch", "failedOrigins":[]});
						return;
					}

					const tos = data.tos;
					const sendData = data.data;
					const failedSockets = [];
					for(let to of tos){
						const toSocket = originToSocketMap[to];
						if(typeof toSocket === "undefined"){
							failedSockets.push(to);
							continue;
						}
						if(typeof sendData === "undefined")continue;
						toSocket.emit("swmsg", sendData);
					}

					if(failedSockets.length === 0){
						res.send({"success":true});
						return;
					}

					res.send({"success":false, "e":"Sockets not found.", "failedOrigins":failedSockets});
				}catch(e){
					res.send({"success":false, "failedOrigins":failedSockets});
				}


			});

		}

		async respondWithExternalEndpoint(originId, server, endpointResource, data, swt/*, updateProgress*/){
			try{
				var authHeader;
				if(typeof swt !== "undefined" && swt !== null){
					authHeader = {"Authorization":"JWT " + swt};
				}
				data._swoid = originId;
				data._swohash = sutils.hashOrigin(originId);

				//TODO: we should be able to work with res.write partial messages so we can use updateProgress
				const result = await sutils.webRequest(server.secure, "POST", authHeader, server.address, "/_/"+endpointResource, data);
				if(!result.success){
					throw result.error;
				}

				return {"success":true, "result":result.result, "id":data.id, "type":result.type};
			}catch(e){
				var errorMessage = e;
				if(typeof e.message !== "undefined")errorMessage = e.message;
				return {"success":false, "error":errorMessage, "id":data.id};
			}
		}

		async respondWithInternalEndpoint(originId, endpointResource, data, userId, updateProgress){

			const endpoint = await sutils.getEndpointByResource(endpointResource);

			try{
				if(typeof endpoint === "undefined"){
					throw `Endpoint ${endpointResource} not found.`;
				}

				endpoint.startDurationTimer();
				var user = null;
				if(endpoint.usesUser && typeof userId !== "undefined" && userId !== null){
					user = await database.read("user", userId);
				}

				const result = await endpoint.process(data, originId, user, updateProgress);
				const returnData = await sutils.getReturnData(result, user);
				returnData.success = true;
				returnData.id = data.id;
				endpoint.stopDurationTimer();
				return returnData;
				
			} catch(e){
				if(typeof endpoint !== "undefined") endpoint.stopDurationTimer();
				var errorMessage = e;
				console.log(e.stack);
				console.log(errorMessage);
				if(typeof e.message !== "undefined")errorMessage = e.message;
				return {"success":false, "error":errorMessage, "id":data.id};
			}
		}

		getServerForEndpoint(endpointResource){
			const endpointServers = this._endpointsMap[endpointResource];
			if(typeof endpointServers === "undefined")throw `Endpoint ${endpointResource} does not exist`;
			return sutils.getRandomElement(endpointServers);
		}

		updateServerEndpoints(graph){
			const endpointsMap = {};
			graph.servers.forEach(function(server){
				//Go through each server and record which endpoints are running
				const serverEndpoints = server.endpoints;
				for(let endpoint of serverEndpoints){
					var endpointServers = endpointsMap[endpoint];
					if(typeof endpointServers === "undefined"){
						endpointServers = [];
						endpointsMap[endpoint] = endpointServers;
					}

					endpointServers.push(server);

				}
			});
			this._endpointsMap = endpointsMap;
		}

		async perform(){
		}
	}

	return MessengerPerformer;
};
