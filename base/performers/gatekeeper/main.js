/*
    This is our gatekeeper script. This code is responsible for:
        - (browser) providing the HTML/JS for connecting to the messenger and getting the page
		- (other) providing just the messenger URL
*/
const path = require("path");
const fs = require("fs");


module.exports = function(sutils) { //This allows us to not have to worry about hard-coding paths, as well as providing this handy utils object

	const Performer = sutils.require("performer.js");
	const Logger = sutils.require("logger.js");

	//The initial page just needs to be able to:
	// - Receive and implement Javascript (Pass ws into relevant JS)
	// - Receive and implement HTML
	// - Receive and implement CSS
	// - 
	

	class GatekeeperPerformer extends Performer {
		
		constructor(){
			super();
			this._resourceSplashDirectories = {};
			this._splashResources = [];
			this._pageTemplates = {};
			this._onFileChanged = this.reloadPageTemplate;
		}

		getPage(resource, messengerAddress){

			var pageTemplate = this._pageTemplates[resource];
			if(typeof pageTemplate === "undefined"){
				pageTemplate = this._defaultPageTemplate;
			}
			pageTemplate = pageTemplate.replace("${messengerAddress}", messengerAddress);
			//pageTemplate = pageTemplate.replace("${baseResource}", resource);
			return pageTemplate;
		}

		readResourceSplashFile(resource, filename, defaultValue = ""){
			const resourceSplashDirectory = this._resourceSplashDirectories[resource];
			if(typeof resourceSplashDirectory === "undefined")return "";
			try{
				const filepath = path.join(resourceSplashDirectory, filename);
				return fs.readFileSync(filepath, { encoding: "utf8" });
			}catch(e){
				return defaultValue;
			}
		}

		reloadPageTemplate(resource){

			var updateResources = typeof resource === "undefined" ? this._splashResources : [resource];

			//TODO: make this use secure sockets, if available
			const jsScript = this.readFile("gate.js");

			const jsScriptMinified = sutils.minifyJs(jsScript);

			const libs = fs.readdirSync(path.join(__dirname, "..", "..", "static", "libs")).filter(function(filename) {
				if(filename === ".DS_Store") return false;
				return true;
			}).map(function(lib){
				return `<script src="/base-static/libs/${lib}"></script>`;
			}).join("");

			for(let resource of updateResources){

				var splashHead = this.readResourceSplashFile(resource, "head.html");
				var splashCss = this.readResourceSplashFile(resource, "style.css");
				var splashBody = this.readResourceSplashFile(resource, "body.html");
				var splashJs = this.readResourceSplashFile(resource, "script.js");

				var splashJsScriptMinified = sutils.minifyJs(splashJs);

				const script = `<span id="sweb-scripts"><script>${splashJsScriptMinified}</script><script>${jsScriptMinified}</script></span>`;
				this._pageTemplates[resource] = `<html><head>${splashHead}${libs}<style>${splashCss}</style></head><body><span id="sweb-body" style="width:100%;height:100%;top:0px;left:0px;position:absolute;">${splashBody}</span>${script}</body></html>`;
			}
			const defaultScript = `<span id="sweb-scripts"><script>${jsScriptMinified}</script></span>`;
			this._defaultPageTemplate = `<html><head>${libs}</head><body><span id="sweb-body" style="width:100%;height:100%;top:0px;left:0px;position:absolute;"></span>${defaultScript}</body></html>`;
			
		}

		async setAppServer(app, server){
			await super.setAppServer(app, server);
			const thiz = this;


			//We can't use the sutils-provided list of webpages/endpoint, as it will 1) include functions and 2) possibly not include any endpoints if those endpoints aren't enabled on the server
			//Eeeeek a hard coded directory!
			const APP_WEBPAGES = path.join(__dirname, "../../../../app/endpoints/webpages/");

			const webpageDirectories = fs.readdirSync(APP_WEBPAGES).filter(function(dir){
				if(dir === ".DS_Store")return false;
				return true;
			});

			for(let webpageDirectory of webpageDirectories){
				//For any URL that isn't an endpoint (a /_/ url), we'll assume it's a browser-based request
				const webpageConfig = JSON.parse(fs.readFileSync(path.join(APP_WEBPAGES, webpageDirectory, "config.json"), { encoding: "utf8" }));
				
				var resource = webpageConfig.resource;
				const constResource = resource.substring(resource.indexOf("/")+1);//We'll trim something like w/lalala to just lalala

				try{
					const resourceSplashDirectory = path.join(APP_WEBPAGES, webpageDirectory, "splash");
					
					//If files in the splash directory change, update our template. 
					fs.watch(resourceSplashDirectory, function (event, filename) {
						thiz.reloadPageTemplate(constResource);
					});

					thiz._splashResources.push(constResource);
					thiz._resourceSplashDirectories[constResource] = resourceSplashDirectory;
					
				}catch(e){
					//If the splash directory doesn't exist, we'll catch the error here. No worries if it doesn't exist.
				}


				//TODO: add SEO rel=canonical tags here
				if(typeof webpageConfig.active !== "undefined" && !webpageConfig.active)continue
				app.get(["/"+constResource, "/"+constResource+"/*"], async function(req, res){
					console.log("Express User for *:");
					console.log(req.user);
					if(typeof thiz.messengers === "undefined" || thiz.messengers.length == 0)throw "We're still getting ready! Just a moment.";
					const chosenMessenger = sutils.getRandomElement(thiz.messengers);
					var response = thiz.getPage(constResource, chosenMessenger);
					res.send(response);
				});
			}

			//Reload all templates
			thiz.reloadPageTemplate();
			

			sutils.listenForChangesToFile("graph", function(graph){thiz.updateMessengers(graph);});

			//this.performPeriod = 5000;
		}

		updateMessengers(graph){
			const messengers = graph.messengers;
			const idToServers = {};
			graph.servers.forEach(function(server){
				idToServers[server.id] = server;
			});
			const newMessengerIps = messengers.map(messengerId => idToServers[messengerId].address);	
			this.messengers = newMessengerIps;
		}

		async perform(){
			Logger.log("GatekeeperPerformer", "Updating messengers list");
			
			
		}
	}

	return GatekeeperPerformer;
};
