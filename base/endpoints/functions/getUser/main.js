

module.exports = function(sutils){

	var DatabaseClass = sutils.requireWithSutils("entity/entityDatabase.js");
	var database = new DatabaseClass();

	const FunctionEndpoint = sutils.requireWithSutils("endpoints/functionEndpoint.js");

	const EntityBroadcast = sutils.requireWithSutils("entity/entityBroadcast.js");

	class GetUserEndpoint extends FunctionEndpoint {

		constructor() {
			super();
			this._broadcast = new EntityBroadcast();
		}

		async ioFunction(inputs, originId, user){
			if(typeof user === "undefined" || user == null)return null;
			await this._broadcast.subscribe("user", user._identifier, originId);
			return user;
		}
	}

	return GetUserEndpoint;

};