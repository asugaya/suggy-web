/*
    This is our scouter. This code is responsible for:
        - Performing a health check on any existing subsets
*/

const passport = require("passport");
module.exports = function(sutils){

	const jwt = require('jsonwebtoken');
	const passportJWT = require("passport-jwt");
	const JWTStrategy   = passportJWT.Strategy;
	const ExtractJWT = passportJWT.ExtractJwt;
	
	var DatabaseClass = sutils.require("entity/entityDatabase.js")(sutils);
	var database = new DatabaseClass();

	const GoogleProvider = require("./providers/google.js")(sutils); 
	const FunctionEndpoint = sutils.require("endpoints/functionEndpoint.js")(sutils);

	class AuthEndpoint extends FunctionEndpoint {

		constructor() {
			super();
			this._providers = [];
		}

		//TODO: accept and save tokens here as well
		async getUserIdFromEmail(email, initialData, tokens, done){

			const filters = [{"key": "email", "value":email}];
			var user;
			try{
				user = await database.querySingle("user", filters);
			} catch(e){
				if(e === "Found no results"){
					const identifier = sutils.uuid();
					const userId = sutils.uuid();
					initialData.email = email;
					user = await database.create("user", identifier, initialData);
					await user.save();

				}else{
					console.log("getUserIdFromEmail");
					console.log(e);
					return done(e);
				}
			}
			if(typeof user.tokens !== "undefined"){
				const swTokens = await user.tokens;
				for(let token of tokens){
					const provider = token.provider;
					swTokens[provider] = {
						"access":token.access
						, "refresh":token.refresh
					}
				}
				await user.save();
			}

			return done(null, {"id":user._identifier});
		}

		requestAuthenticated(req, res, redirectUrl){
			const config = sutils.config;
			//res.redirect(req.query.state);
			const jwtData = {"user":req.user};
		    const token = jwt.sign(jwtData, config.secretTwo);
		    var installTokenScript = "<script>";
		    installTokenScript += "window.localStorage.setItem('swt', '"+token+"');"
		    installTokenScript += "window.location.href='"+redirectUrl+"'";
		    installTokenScript += "</script>";


   			res.write(installTokenScript);
		}

		async setAppServer(app, server){
			const config = sutils.config;

			app.use(passport.initialize());
			//app.use(passport.session());

			var callbackUrlBase = "";
			callbackUrlBase += config.secure ? "https://" : "http://";
			callbackUrlBase += config.domain;

			const gProvider = new GoogleProvider();
			await gProvider.setupPassport(callbackUrlBase, this.getUserIdFromEmail);
			gProvider.setupEndpoints(app, this.requestAuthenticated);

			app.get('/logout', function(req, res){
				req.logout();
				console.log("LOGGING OUT! :)")

				var targetURL = req.query.targetURL
				if(typeof targetURL === "undefined")targetURL = "/";
				res.redirect(targetURL);
			});

			this.setupJWT();
		}

		setupJWT(){
			const config = sutils.config;
			passport.use(new JWTStrategy({
		        jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('JWT')
		        , secretOrKey   : config.secretTwo
		        , ignoreExpiration: true//TODO: don't ignore expiration
			    },
			    function (jwtPayload, cb) {
			    	console.log("JWT PAYLOAD:");
			    	console.log(jwtPayload);
			    	cb(null, jwtPayload.user)
			    }
			));
		}

	}

	return AuthEndpoint;

};